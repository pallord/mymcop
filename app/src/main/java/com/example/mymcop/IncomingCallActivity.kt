package com.example.mymcop

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import org.doubango.ngn.NgnEngine
import org.doubango.ngn.events.NgnInviteEventArgs
import org.doubango.ngn.events.NgnInviteEventTypes
import org.doubango.ngn.events.NgnMediaPluginEventArgs
import org.doubango.ngn.model.NgnContact
import org.doubango.ngn.sip.NgnAVSession
import org.doubango.ngn.sip.NgnInviteSession
import org.doubango.ngn.utils.NgnStringUtils
import org.doubango.ngn.utils.NgnUriUtils

class IncomingCallActivity : AppCompatActivity() {

    private lateinit var mAVSession: NgnAVSession
    private var mRemotePartyDisplayName: String? = null
    private lateinit var mEngine: NgnEngine
    private var mAVTransfSession: NgnAVSession? = null

    private var mSipBroadCastRecv: BroadcastReceiver? = null

    private lateinit var btnAcceptCall: Button
    private lateinit var btnHangupCall: Button
    private lateinit var tvName: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_incoming_call)
        mEngine = NgnEngine.getInstance()

        btnAcceptCall = findViewById(R.id.btnAcceptCall)
        btnHangupCall = findViewById(R.id.btnHangUpCall)
        tvName = findViewById(R.id.tvName)

        val sessionId = intent.getLongExtra(EXTRA_SESSION_ID,-1)
        if(sessionId == -1L){
            Log.e(TAG, "Invalid audio/video session")
            finish()
        }

        mAVSession = NgnAVSession.getSession(sessionId)
        mAVSession.incRef()
        mAVSession.context = this

        //val remoteParty = NgnContact(1, "Test")
        val remoteParty: NgnContact = mEngine.contactService.getContactByUri(mAVSession.remotePartyUri)
        if(remoteParty !=null){
            mRemotePartyDisplayName = remoteParty.displayName
        }
        else { mRemotePartyDisplayName = NgnUriUtils.getDisplayName(mAVSession.remotePartyUri) }
        if (NgnStringUtils.isNullOrEmpty(mRemotePartyDisplayName)) {
            mRemotePartyDisplayName = "Unknown"
        }

        tvName.text = mRemotePartyDisplayName
        NgnEngine.getInstance()

        mSipBroadCastRecv = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                if (NgnInviteEventArgs.ACTION_INVITE_EVENT == intent.action) {
                    handleSipEvent(intent)
                } else if (NgnMediaPluginEventArgs.ACTION_MEDIA_PLUGIN_EVENT == intent.action) {
                    handleMediaEvent(intent)
                }
            }
        }
        val intentFilter = IntentFilter()
        intentFilter.addAction(NgnInviteEventArgs.ACTION_INVITE_EVENT)
        intentFilter.addAction(NgnMediaPluginEventArgs.ACTION_MEDIA_PLUGIN_EVENT)
        registerReceiver(mSipBroadCastRecv, intentFilter)
    }

    private fun handleMediaEvent(intent: Intent) {
        val action = intent.action

        if (NgnMediaPluginEventArgs.ACTION_MEDIA_PLUGIN_EVENT == action) {
            val args =
                intent.getParcelableExtra<NgnMediaPluginEventArgs>(NgnMediaPluginEventArgs.EXTRA_EMBEDDED)
            if (args == null) {
                Log.e(TAG, "Invalid event args")
                return
            }
        }
    }

    private fun handleSipEvent(intent: Intent) {
        if (mAVSession == null) {
            Log.e(TAG, "Invalid session object")
            return
        }
        val action = intent.action
        if (NgnInviteEventArgs.ACTION_INVITE_EVENT == action) {
            val args =
                intent.getParcelableExtra<NgnInviteEventArgs>(NgnInviteEventArgs.EXTRA_EMBEDDED)
            if (args == null) {
                Log.e(TAG, "Invalid event args")
                return
            }
            if (args.sessionId != mAVSession.id) {
                if (args.eventType == NgnInviteEventTypes.REMOTE_TRANSFER_INPROGESS) {
                    // Native code created new session handle to be used to replace the current one (event = "tsip_i_ect_newcall").
                    mAVTransfSession = NgnAVSession.getSession(args.sessionId)
                }
                return
            }
            val state = mAVSession.state
            when (state) {
                NgnInviteSession.InviteState.TERMINATED, NgnInviteSession.InviteState.TERMINATING ->{

                    finish()
                }
                NgnInviteSession.InviteState.EARLY_MEDIA, NgnInviteSession.InviteState.INCALL -> {
                    // stop using the speaker (also done in ServiceManager())
                    mEngine.soundService.stopRingTone()
                    mAVSession.setSpeakerphoneOn(false)

                    when (args.eventType) {
                        NgnInviteEventTypes.REMOTE_DEVICE_INFO_CHANGED -> {
                            Log.d(
                                TAG,
                                String.format(
                                    "Remote device info changed: orientation: %s",
                                    mAVSession.remoteDeviceInfo.orientation
                                )
                            )
                        }
                        NgnInviteEventTypes.MEDIA_UPDATED -> Log.d(TAG,"Call transfer: Media Updated")
                        NgnInviteEventTypes.LOCAL_TRANSFER_TRYING -> Log.d(TAG,"Call Transfer: Initiated")
                        NgnInviteEventTypes.LOCAL_TRANSFER_FAILED -> Log.d(TAG,"Call Transfer: Failed")
                        NgnInviteEventTypes.LOCAL_TRANSFER_ACCEPTED -> Log.d(TAG,"Call Transfer: Accepted")
                        NgnInviteEventTypes.LOCAL_TRANSFER_COMPLETED -> Log.d(TAG,"Call Transfer: Completed")
                        NgnInviteEventTypes.LOCAL_TRANSFER_NOTIFY, NgnInviteEventTypes.REMOTE_TRANSFER_NOTIFY -> {
                            if (mAVSession != null) {
                                val sipCode = intent.getShortExtra(
                                    NgnInviteEventArgs.EXTRA_SIPCODE,
                                    0.toShort()
                                )
                                if (sipCode >= 300 && mAVSession.isLocalHeld) {
                                    mAVSession.resumeCall()
                                }
                            }
                        }

                        NgnInviteEventTypes.REMOTE_TRANSFER_FAILED -> {
                            mAVTransfSession = null
                        }
                        NgnInviteEventTypes.REMOTE_TRANSFER_COMPLETED -> {
                            if (mAVTransfSession != null) {
                                mAVTransfSession!!.setContext(mAVSession.context)
                                mAVSession = mAVTransfSession!!
                                mAVTransfSession = null
                                // loadInCallView(true)
                            }
                        }
                        else -> {
                        }
                    }
                }
                else -> {
                }
            }
        }
    }

    fun acceptCallBtn_onClick (view: View){
        acceptCall()
        view.isEnabled=false
    }
    fun hangUpCallBtn_onClick (view: View){
        hangUpCall()
        finish()
    }

    private fun hangUpCall(): Boolean {
        return if (mAVSession != null) {
            mAVSession.hangUpCall()
        } else false
    }

    private fun acceptCall(): Boolean {
        return if (mAVSession != null) {
            mAVSession.acceptCall()
        } else false
    }

    override fun onDestroy() {
        // release the listener
        if (mSipBroadCastRecv != null) {
            unregisterReceiver(mSipBroadCastRecv)
            mSipBroadCastRecv = null
        }
        super.onDestroy()
    }

    companion object{
        private val TAG = IncomingCallActivity::class.java.canonicalName

        val EXTRA_SESSION_ID = "session_id"
        fun getIntent(context: Context, sessionId: Long): Intent {
            val intent = Intent(context,IncomingCallActivity::class.java)
            intent.putExtra(EXTRA_SESSION_ID,sessionId)
            return intent
        }
    }
}
