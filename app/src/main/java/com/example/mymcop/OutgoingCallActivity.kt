package com.example.mymcop

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import org.doubango.ngn.NgnEngine
import org.doubango.ngn.events.NgnInviteEventArgs
import org.doubango.ngn.sip.NgnAVSession
import org.doubango.ngn.sip.NgnInviteSession

class OutgoingCallActivity : AppCompatActivity() {
    private val TAG = OutgoingCallActivity::class.java!!.getCanonicalName()

    private lateinit var mEngine: NgnEngine
    private var mTvInfo: TextView? = null
    private var mTvRemote: TextView? = null
    private var mBtHangUp: Button? = null

    private var mSession: NgnAVSession? = null
    private var mSipBroadCastRecv: BroadcastReceiver? = null

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_outgoing_call)
        mEngine = NgnEngine.getInstance()

        val extras = intent.extras
        if (extras != null) {
            mSession = NgnAVSession.getSession(extras.getLong(MainActivity.EXTRAT_SIP_SESSION_ID))
        }

        if (mSession == null) {
            Log.e(TAG, "Null session")
            finish()
            return
        }
        mSession!!.incRef()
        mSession!!.context = this

        // listen for audio/video session state
        mSipBroadCastRecv = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                handleSipEvent(intent)
            }
        }
        val intentFilter = IntentFilter()
        intentFilter.addAction(NgnInviteEventArgs.ACTION_INVITE_EVENT)
        registerReceiver(mSipBroadCastRecv, intentFilter)

        mTvInfo = findViewById(R.id.call_screen_textView_info) as TextView
        mTvRemote = findViewById(R.id.callscreen_textView_remote) as TextView
        mBtHangUp = findViewById(R.id.callscreen_button_hangup) as Button

        mBtHangUp!!.setOnClickListener {
            if (mSession != null) {
                mSession!!.hangUpCall()
            }
        }

        mTvRemote!!.text = mSession!!.remotePartyDisplayName
        mTvInfo!!.text = getStateDesc(mSession!!.state)
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG, "onResume()")
        if (mSession != null) {
            val callState = mSession!!.state
            mTvInfo!!.text = getStateDesc(callState)
            if (callState == NgnInviteSession.InviteState.TERMINATING || callState == NgnInviteSession.InviteState.TERMINATED) {
                finish()
            }
        }
    }

    override fun onDestroy() {
        Log.d(TAG, "onDestroy()")
        if (mSipBroadCastRecv != null) {
            unregisterReceiver(mSipBroadCastRecv)
            mSipBroadCastRecv = null
        }

        if (mSession != null) {
            mSession!!.context = null
            mSession!!.decRef()
        }
        super.onDestroy()
    }

    private fun getStateDesc(state: NgnInviteSession.InviteState): String {
        when (state) {
            NgnInviteSession.InviteState.NONE -> return "Unknown"
            NgnInviteSession.InviteState.INCOMING -> return "Incoming"
            NgnInviteSession.InviteState.INPROGRESS -> return "Inprogress"
            NgnInviteSession.InviteState.REMOTE_RINGING -> return "Ringing"
            NgnInviteSession.InviteState.EARLY_MEDIA -> return "Early media"
            NgnInviteSession.InviteState.INCALL -> return "In Call"
            NgnInviteSession.InviteState.TERMINATING -> return "Terminating"
            NgnInviteSession.InviteState.TERMINATED -> return "termibated"
            else -> return "Unknown"
        }
    }

    private fun handleSipEvent(intent: Intent) {
        if (mSession == null) {
            Log.e(TAG, "Invalid session object")
            return
        }
        val action = intent.action
        if (NgnInviteEventArgs.ACTION_INVITE_EVENT == action) {
            val args =
                intent.getParcelableExtra<NgnInviteEventArgs>(NgnInviteEventArgs.EXTRA_EMBEDDED)
            if (args == null) {
                Log.e(TAG, "Invalid event args")
                return
            }
            if (args.sessionId != mSession!!.id) {
                return
            }

            val callState = mSession!!.state
            mTvInfo!!.text = getStateDesc(callState)
            when (callState) {
                NgnInviteSession.InviteState.REMOTE_RINGING -> mEngine.soundService.startRingBackTone()
                NgnInviteSession.InviteState.INCOMING -> mEngine.soundService.startRingTone()
                NgnInviteSession.InviteState.EARLY_MEDIA, NgnInviteSession.InviteState.INCALL -> {
                    mEngine.soundService.stopRingTone()
                    mEngine.soundService.stopRingBackTone()
                    mSession!!.setSpeakerphoneOn(false)
                }
                NgnInviteSession.InviteState.TERMINATING, NgnInviteSession.InviteState.TERMINATED -> {
                    mEngine.soundService.stopRingTone()
                    mEngine.soundService.stopRingBackTone()
                    finish()
                }
                else -> {
                }
            }
        }
    }
}
